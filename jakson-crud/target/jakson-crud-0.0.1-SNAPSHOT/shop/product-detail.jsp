<%--
  Created by IntelliJ IDEA.
  User: bonng
  Date: 7/15/18
  Time: 00:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Product Detail</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../resources/images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/css/util.css">
    <link rel="stylesheet" type="text/css" href="../resources/css/main.css">
    <!--===============================================================================================-->
</head>
<body class="animsition">

<!-- Header -->
<jsp:include page="/shop/header.jsp" />


<%-- breadcrumb --%>
<%--<div class="bread-crumb bgwhite flex-w p-l-52 p-r-15 p-t-30 p-l-15-sm">--%>
    <%--<a href="index.html" class="s-text16">--%>
        <%--Home--%>
        <%--<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>--%>
    <%--</a>--%>

    <%--<a href="product.html" class="s-text16">--%>
        <%--Women--%>
        <%--<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>--%>
    <%--</a>--%>

    <%--<a href="#" class="s-text16">--%>
        <%--T-Shirt--%>
        <%--<i class="fa fa-angle-right m-l-8 m-r-9" aria-hidden="true"></i>--%>
    <%--</a>--%>

    <%--<span class="s-text17">--%>
			<%--Boxy T-Shirt with Roll Sleeve Detail--%>
		<%--</span>--%>
<%--</div>--%>

<!-- Product Detail -->
<div class="container bgwhite p-t-35 p-b-80">
    <div class="flex-w flex-sb">
        <div class="w-size13 p-t-30 respon5">
            <div class="wrap-slick3 flex-sb flex-w">
                <div class="wrap-slick3-dots"></div>

                <div class="slick3">
                    <div class="item-slick3" data-thumb="../resources/images/thumb-item-01.jpg">
                        <div class="wrap-pic-w">
                            <img src="../resources/images/product-detail-01.jpg" alt="IMG-PRODUCT">
                        </div>
                    </div>

                    <div class="item-slick3" data-thumb="../resources/images/thumb-item-02.jpg">
                        <div class="wrap-pic-w">
                            <img src="../resources/images/product-detail-02.jpg" alt="IMG-PRODUCT">
                        </div>
                    </div>

                    <div class="item-slick3" data-thumb="../resources/images/thumb-item-03.jpg">
                        <div class="wrap-pic-w">
                            <img src="../resources/images/product-detail-03.jpg" alt="IMG-PRODUCT">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="w-size14 p-t-30 respon5">
            <h4 class="product-detail-name m-text16 p-b-13">
                ${product.name}
            </h4>

            <span class="m-text17">
                $${product.price}
				</span>

            <%--<p class="s-text8 p-t-10">--%>
                <%--Nulla eget sem vitae eros pharetra viverra. Nam vitae luctus ligula. Mauris consequat ornare feugiat.--%>
            <%--</p>--%>

            <!--  -->
            <div class="p-t-33 p-b-60">

                <div class="flex-r-m flex-w p-t-10">
                    <div class="w-size16 flex-m flex-w">
                        <div class="flex-w bo5 of-hidden m-r-22 m-t-10 m-b-10">
                            <button class="btn-num-product-down color1 flex-c-m size7 bg8 eff2">
                                <i class="fs-12 fa fa-minus" aria-hidden="true"></i>
                            </button>

                            <input class="size8 m-text18 t-center num-product" type="number" name="num-product" value="1">

                            <button class="btn-num-product-up color1 flex-c-m size7 bg8 eff2">
                                <i class="fs-12 fa fa-plus" aria-hidden="true"></i>
                            </button>
                        </div>

                        <div productId="${product.id}" class="btn-addcart-product-detail size9 trans-0-4 m-t-10 m-b-10">
                            <!-- Button -->
                            <button class="flex-c-m sizefull bg1 bo-rad-23 hov1 s-text1 trans-0-4">
                                Add to Cart
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="p-b-45">
                <span class="s-text8 m-r-35">SKU: ${product.id}</span>
                <span class="s-text8">Categories: ${product.categoryId}</span>
            </div>

            <!--  -->
            <div class="wrap-dropdown-content bo6 p-t-15 p-b-14 active-dropdown-content">
                <h5 class="js-toggle-dropdown-content flex-sb-m cs-pointer m-text19 color0-hov trans-0-4">
                    Description
                    <i class="down-mark fs-12 color1 fa fa-minus dis-none" aria-hidden="true"></i>
                    <i class="up-mark fs-12 color1 fa fa-plus" aria-hidden="true"></i>
                </h5>

                <div class="dropdown-content dis-none p-t-15 p-b-23">
                    <p class="s-text8">
                        ${product.description}
                    </p>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Footer -->
<jsp:include page="/shop/footer.jsp" />

<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="../resources/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/select2/select2.min.js"></script>
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect2')
    });
</script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="../resources/js/slick-custom.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/sweetalert/sweetalert.min.js"></script>
<script type="text/javascript" src="../resources/js/shop/product-detail.js"></script>

<!--===============================================================================================-->
<script src="../resources/js/main.js"></script>

</body>
</html>

