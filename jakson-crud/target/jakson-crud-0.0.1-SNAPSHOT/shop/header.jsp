<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: bonng
  Date: 7/15/18
  Time: 05:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Header -->
<header class="header1">
    <!-- Header desktop -->
    <div class="container-menu-header">
        <div class="topbar">
            <div class="topbar-social">
                <a href="#" class="topbar-social-item fa fa-facebook"></a>
                <a href="#" class="topbar-social-item fa fa-instagram"></a>
                <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
            </div>

            <span class="topbar-child1">
					Free shipping for standard order over $100
				</span>

            <div class="topbar-child2">
					<span class="topbar-email">
						mobileshop@sbm.com
					</span>

                <div class="topbar-language rs1-select2">
                    <select class="selection-1" name="time">
                        <option>USD</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="wrap_header">
            <!-- Logo -->
            <a href="../index.jsp" class="logo">
                <img src="../resources/images/icons/shop_logo.jpg" alt="IMG-LOGO">
            </a>

            <!-- Menu -->
            <div class="wrap_menu">
                <nav class="menu">
                    <ul class="main_menu">
                        <li class=" <c:if test="${activePage == 'home'}">
                            <c:out value="sale-noti"/></c:if>">
                            <a href="../index.jsp">Home</a>
                            <%--<ul class="sub_menu">--%>
                            <%--<li><a href="index.html">Homepage V1</a></li>--%>
                            <%--<li><a href="home-02.html">Homepage V2</a></li>--%>
                            <%--<li><a href="home-03.html">Homepage V3</a></li>--%>
                            <%--</ul>--%>
                        </li>

                        <li class=" <c:if test="${activePage == 'product'}">
                            <c:out value="sale-noti"/></c:if>">
                            <a href="/shop/product">Shop</a>
                        </li>

                        <li class=" <c:if test="${activePage == 'about'}">
                            <c:out value="sale-noti"/></c:if>">
                            <a href="about.html">About</a>
                        </li>

                        <li class=" <c:if test="${activePage == 'contact'}">
                            <c:out value="sale-noti"/></c:if>">
                            <a href="contact.html">Contact</a>
                        </li>
                    </ul>
                </nav>
            </div>

            <!-- Header Icon -->
            <div class="header-icons">
                <a href="#" class="header-wrapicon1 dis-block">
                    <img src="../resources/images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide1"></span>

                <div class="header-wrapicon2">
                    <img src="../resources/images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown"
                         alt="ICON">
                    <span class="numProduct header-icons-noti">${numProduct}</span>

                    <!-- Header cart noti -->
                    <div class="header-cart header-dropdown">
                        <div class="header-cart-buttons">
                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="/shop/cart" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    View Cart
                                </a>
                            </div>

                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    Check Out
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Header Mobile -->
    <div class="wrap_header_mobile">
        <!-- Logo moblie -->
        <a href="../index.jsp" class="logo-mobile">
            <img src="../resources/images/icons/shop_logo.jpg" alt="IMG-LOGO">
        </a>

        <!-- Button show menu -->
        <div class="btn-show-menu">
            <!-- Header Icon mobile -->
            <div class="header-icons-mobile">
                <a href="#" class="header-wrapicon1 dis-block">
                    <img src="../resources/images/icons/icon-header-01.png" class="header-icon1" alt="ICON">
                </a>

                <span class="linedivide2"></span>

                <div class="header-wrapicon2">
                    <img src="../resources/images/icons/icon-header-02.png" class="header-icon1 js-show-header-dropdown"
                         alt="ICON">
                    <span class="cartQty header-icons-noti">${numProduct}</span>

                    <!-- Header cart noti -->
                    <div class="header-cart header-dropdown">
                        <c:if test="${cartSubtotal > 0.0}">
                            <ul class="header-cart-wrapitem">
                                <c:forEach items="${productLines}" var="productLine">
                                    <li class="header-cart-item">
                                        <div class="header-cart-item-img">
                                            <img src="../resources/images/item-cart-01.jpg" alt="IMG">
                                        </div>

                                        <div class="header-cart-item-txt">
                                            <a href="#" class="header-cart-item-name">
                                                    ${productLine.product.name}
                                            </a>

                                            <span class="header-cart-item-info">
                                                ${productLine.qty} x $ ${productLine.product.price}
										</span>
                                        </div>
                                    </li>
                                </c:forEach>

                            </ul>

                            <div class="header-cart-total">
                                Total: $${cartSubtotal}
                            </div>
                        </c:if>
                        <div class="header-cart-buttons">
                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="/shop/cart" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    View Cart
                                </a>
                            </div>

                            <div class="header-cart-wrapbtn">
                                <!-- Button -->
                                <a href="#" class="flex-c-m size1 bg1 bo-rad-20 hov1 s-text1 trans-0-4">
                                    Check Out
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
            </div>
        </div>
    </div>

    <!-- Menu Mobile -->
    <div class="wrap-side-menu">
        <nav class="side-menu">
            <ul class="main-menu">
                <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
						<span class="topbar-child1">
							Free shipping for standard order over $100
						</span>
                </li>

                <li class="item-topbar-mobile p-l-20 p-t-8 p-b-8">
                    <div class="topbar-child2-mobile">
							<span class="topbar-email">
								mobileshop@smb.com
							</span>

                        <div class="topbar-language rs1-select2">
                            <select class="selection-1" name="time">
                                <option>USD</option>
                            </select>
                        </div>
                    </div>
                </li>

                <li class="item-topbar-mobile p-l-10">
                    <div class="topbar-social-mobile">
                        <a href="#" class="topbar-social-item fa fa-facebook"></a>
                        <a href="#" class="topbar-social-item fa fa-instagram"></a>
                        <a href="#" class="topbar-social-item fa fa-pinterest-p"></a>
                        <a href="#" class="topbar-social-item fa fa-snapchat-ghost"></a>
                        <a href="#" class="topbar-social-item fa fa-youtube-play"></a>
                    </div>
                </li>

                <li class="item-menu-mobile">
                    <a href="index.jsp">Home</a>
                    <%--<ul class="sub-menu">--%>
                    <%--<li><a href="../index.jsp">Homepage V1</a></li>--%>
                    <%--<li><a href="home-02.html">Homepage V2</a></li>--%>
                    <%--<li><a href="home-03.html">Homepage V3</a></li>--%>
                    <%--</ul>--%>
                    <i class="arrow-main-menu fa fa-angle-right" aria-hidden="true"></i>
                </li>

                <li class="item-menu-mobile">
                    <a href="product.jsp">Shop</a>
                </li>

                <%--<li class="item-menu-mobile">--%>
                <%--<a href="product.jsp">Sale</a>--%>
                <%--</li>--%>

                <li class="item-menu-mobile">
                    <a href="/shop/cart">Features</a>
                </li>

                <%--<li class="item-menu-mobile">--%>
                <%--<a href="blog.html">Blog</a>--%>
                <%--</li>--%>

                <li class="item-menu-mobile">
                    <a href="about.html">About</a>
                </li>

                <li class="item-menu-mobile">
                    <a href="contact.html">Contact</a>
                </li>
            </ul>
        </nav>
    </div>
</header>

