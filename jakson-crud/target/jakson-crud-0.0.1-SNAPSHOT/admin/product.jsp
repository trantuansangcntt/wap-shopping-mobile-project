<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <title>Add New Contact</title>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="js/product.js"></script>
</head>
<body>
<table id="tbl_products">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Price</th>
        <th>Image</th>
        <th>Description</th>
        <th>Amount Available</th>
        <th>Category</th>
        <th>Active</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${products}" var="product">
        <tr>
            <td><c:out value="${product.id}"/></td>
            <td><c:out value="${product.name}"/></td>
            <td><c:out value="${product.price}"/></td>
            <td><c:out value="${product.image}"/></td>
            <td><c:out value="${product.description}"/></td>
            <td><c:out value="${product.amountAvailable}"/></td>
            <td><c:out value="${product.categoryId}"/></td>
            <td><input type="checkbox" disabled <c:out value="${product.active == 0 ? 'unchecked' : 'checked'}"/> /></td>
        </tr>
    </c:forEach>
    </tbody>
</table>


<fieldset>
    <div>
        <label for="txtId">Id</label> <input type="text" id="txtId"
                                             readonly="readonly" placeholder="Id"/>
    </div>
    <div>
        <label for="txtName">Name</label> <input type="text" id="txtName"
                                                 placeholder="Name"/>
    </div>
    <div>
        <label for="txtPrice">Price</label> <input type="text" id="txtPrice"
                                                   placeholder="Price"/>
    </div>

    <div>
        <label for="txtImage">Image</label> <input type="text" id="txtImage"
                                                   placeholder="Product picture"/>
    </div>
    <div>
        <label for="txtDescription">Description</label> <textarea type="text" id="txtDescription" cols="40" rows="15"
                                                                  placeholder="Description"> </textarea>
    </div>
    <div>
        <label for="txtAmountAvailable">Available amount</label> <input type="text" id="txtAmountAvailable"
                                                                        placeholder="Available amount"/>
    </div>
    <div>
        <label for="txtCategoryId">Category</label> <input type="text" id="txtCategoryId"
                                                           placeholder="CategoryId"/>
    </div>

    <div>
        <label for="chkActive">Active</label> <input type="checkbox" id="chkActive" />
    </div>

    <div>
        <input id="btnAddProduct" type="submit" value="Submit"/>
    </div>
</fieldset>
</body>
</html>