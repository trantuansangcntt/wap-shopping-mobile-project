$(function () {
    /*[ No ui ]
    ===========================================================*/
    var filterBar = $('#filter-bar')[0];
    var min = parseInt($('#value-lower').text());
    var max = parseInt($('#value-upper').text());

    noUiSlider.create(filterBar, {
        start: [min, max],
        connect: true,
        range: {
            'min': min,
            'max': max
        }
    });

    var skipValues = [$('#value-lower')[0], $('#value-upper')[0]];

    filterBar.noUiSlider.on('update', function (values, handle) {
        skipValues[handle].innerHTML = Math.round(values[handle]);
    });

    $('.block2-btn-addcart').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            let param = {
                requestType: "ADD-PRODUCT-TO-CART",
                productId: $(this).attr("productId"),
                qty: 1,
            };
            console.log(param);
            $.post("/shop/cart", param)
                .done(function (data) {
                    $(".numProduct").text(JSON.parse(data).numProduct);
                    swal(nameProduct, "is added to cart !", "success");
                    // updateMenuCheckout();
                })
                .fail(function () {
                    swal(nameProduct, "is failed to add to cart !", "error");
                });
        });
    });

    $('.block2-btn-addwishlist').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });

    $(".bntSearch").each(function () {
        $(this).on('click', function () {
            var searchText = {searchText: $(".txtSearch").val()};

            $.post("/shop/product", searchText).done(function () {
                $(".row").reload(" .row");
            });
        });
    })

    function updateMenuCheckout() {
        $(".header-wrapicon2").append(
            $("<div>").addClass("header-cart header-dropdown").append(
                $("<ul>").addClass("header-cart-wrapitem").append(
                    $("<li>").addClass("header-cart-item").append(
                        $("<div>").addClass("header-cart-item-img").append(
                            $("<img>").attr({src: "../resources/images/item-cart-01.jpg",alt: "IMG"})
                        )
                    ).append(
                        $("<div>").addClass("header-cart-item-txt").append(
                            $("<a>").addClass("header-cart-item-name").attr({href:"#"}).text("${productLine.product.name}")
                        ).append(
                            $("<span>").addClass("header-cart-item-info").text("${productLine.qty} x ${productLine.product.price}")
                        )
                    )
                )
            ).append(
                $("<div>").addClass("header-cart-total").text("Total: $${total}")
            )
        );
    }

})

