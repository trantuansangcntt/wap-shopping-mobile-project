$(function () {
    $('.btn-addcart-product-detail').each(function () {
        var nameProduct = $(".product-detail-name").html();
        $(this).on('click', function () {
            let param = {
                requestType: "ADD-PRODUCT-TO-CART",
                productId: $(this).attr("productId"),
                qty: 1,
            };
            $.post("/shop/cart", param)
                .done(function (data) {
                    $(".numProduct").text(JSON.parse(data).numProduct);
                    swal(nameProduct, "is added to cart !", "success");
                })
                .fail(function () {
                    swal(nameProduct, "is failed to add to cart !", "error");
                });
        });
    });

    $('.block2-btn-addwishlist').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });
})