$(function () {

    $('.btnUpdateCart').each(function () {
        $(this).on('click', function () {
            var param = {
                requestType: "UPDATE-CART",
                data: [],
            };
            $(".txtProductQty").each(function (key, value) {
                param.data.push({
                    productId: $(this).attr("productId"),
                    qty: $(this).attr("value")
                });
            });

            console.log(param);

            // $.post("/shop/cart", param)
            //     .done(function (data) {
            //         $(".numProduct").text(JSON.parse(data).numProduct);
            //         swal("SUCCESS", "Your cart is updated !", "success");
            //     })
            //     .fail(function () {
            //         swal("FAILED", "Failed to update your cart !", "error");
            //     });
        });
    });

    $('.btnRemoveProduct').each(function () {
        $(this).on('click', function () {
            let rowId = $(this).attr("productId");
            $("#row-" + rowId).remove();
        });
    });

})