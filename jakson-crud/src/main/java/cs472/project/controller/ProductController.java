package cs472.project.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cs472.project.Entities.Product;
import cs472.project.Entities.ProductService;
import cs472.project.dao.ProductDAO;

/**
 * Servlet implementation class ProductController
 */
@WebServlet({ "/admin/product"})
public class ProductController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	public void init() throws ServletException {
		ProductDAO.getInstance().loadProductsDB(getServletContext().getRealPath("/WEB-INF"));
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("products", ProductDAO.getInstance().getAllProducts());
		RequestDispatcher view = request.getRequestDispatcher("product.jsp");
		view.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Product product = ProductService.getInstance().productFromJSON(request.getParameter("product"));
		product.setId(ProductDAO.getInstance().genId());
		ProductDAO.getInstance().addProduct(product);
		
		PrintWriter out =response.getWriter();
		out.print(ProductService.getInstance().productToJSON(product));

	}

}
