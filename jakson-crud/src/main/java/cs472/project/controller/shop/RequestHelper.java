package cs472.project.controller.shop;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cs472.project.Entities.*;
import cs472.project.controller.shop.bean.RequestParam;
import cs472.project.controller.shop.bean.ResponseParam;
import cs472.project.controller.shop.bean.SearchProductParam;
import cs472.project.controller.shop.bean.SearchProductResponseParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

public class RequestHelper {

    public static void createCutomerCart(HttpServletRequest request, HttpServletRequest response) {

    }

    public static RequestParam getRequestParam(HttpServletRequest request, String paramName) {
        RequestParam result = null;
        if(request.getParameter(paramName) != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                result = mapper.readValue(request.getParameter(paramName), RequestParam.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static String responseToJSON(Object param) {
        String result = "";
        try {
            result = new ObjectMapper().writeValueAsString(param);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ResponseParam createResponseParam() {
        return new ResponseParam();
    }

    public static SearchProductResponseParam createSearchProductResponseParam() {
        return new SearchProductResponseParam();
    }

    public static SearchProductParam getSearchRequestParam(HttpServletRequest request, String paramName) {
        SearchProductParam result = null;
        if(request.getParameter(paramName) != null) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                result = mapper.readValue(request.getParameter(paramName), SearchProductParam.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    public static void initAttributesForRequest(HttpServletRequest request) {
//        setProductsAttribute(request, ProductService.getInstance().getAllProducts());
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute("cart") == null) {
            setCartSubtotal(request, 0);
            setCartNumberOfProduct(request, 0);
        }
    }

    public static void setDefaultFilterParameters(HttpServletRequest request) {
        request.setAttribute("filterMin", 0);
        request.setAttribute("filterMax", 2000);
    }

    public static void setCurrentPage(HttpServletRequest request, String location) {
        request.getSession().setAttribute("activePage", location);
    }

    public static void setCartSubtotal(HttpServletRequest request, double subtotal) {
        request.getSession().setAttribute("cartSubtotal", subtotal);
    }

    public static void setCartNumberOfProduct(HttpServletRequest request, int num) {
        request.getSession().setAttribute("numberOfProduct", num);
    }

    public static void setProductsAttribute(HttpServletRequest request, List<Product> products) {
        request.setAttribute("products", products);
        request.setAttribute("totalProduct", products.size());
    }

    public static void setProductLineAttribute(HttpServletRequest request, List<ProductLine> productLines) {
        request.setAttribute("productLines", productLines);
    }

    public static Cart getSessionCartAttribute(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session != null && session.getAttribute("cart") != null) {
            return (Cart) session.getAttribute("cart");
        }
        return null;
    }

    public static void createSessionCartAttribute(HttpServletRequest request) {
        request.getSession().setAttribute("cart", CartService.getInstance().createCart());
    }

    public static void setProductAttribute(HttpServletRequest request, Product product) {
        request.setAttribute("product", product);
    }
}
