package cs472.project.controller.shop;

import cs472.project.Entities.Cart;
import cs472.project.Entities.Product;
import cs472.project.Entities.ProductService;
import cs472.project.dao.ProductDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet( "/shop/product-detail")
public class ProductDetailController extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        double cartSubtotal = 0.0;
        int numberOfProduct = 0;
        Cart cart = RequestHelper.getSessionCartAttribute(request);
        if(cart != null) {
            RequestHelper.setProductLineAttribute(request, cart.getProductLines());
            cartSubtotal = cart.getTotal();
            numberOfProduct = cart.getTotalProduct();
        }

        int productId = Integer.parseInt(request.getParameter("productId"));
        RequestHelper.setProductAttribute(request, ProductService.getInstance().getProductById(productId));

        RequestHelper.setCartSubtotal(request, cartSubtotal);
        RequestHelper.setCartNumberOfProduct(request, numberOfProduct);
        RequestHelper.setCurrentPage(request, "product");
        request.getRequestDispatcher("/shop/product-detail.jsp").forward(request, response);
    }
}
