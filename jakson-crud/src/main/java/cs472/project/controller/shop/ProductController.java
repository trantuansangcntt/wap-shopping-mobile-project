package cs472.project.controller.shop;

import cs472.project.Entities.Cart;
import cs472.project.Entities.Product;
import cs472.project.Entities.ProductService;
import cs472.project.controller.shop.bean.RequestParam;
import cs472.project.controller.shop.bean.ResponseParam;
import cs472.project.controller.shop.bean.SearchProductParam;
import cs472.project.controller.shop.bean.SearchProductResponseParam;
import cs472.project.dao.ProductDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@WebServlet("/shop/product")
public class ProductController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        SearchProductParam param = RequestHelper.getSearchRequestParam(request, "param");
        if (param != null) {
            System.out.println("Found param to search product: ");
            System.out.println(param.toString());
            SearchProductResponseParam respParam = RequestHelper.createSearchProductResponseParam();
            respParam.setProducts(ProductService.getInstance().searchProducts(param));
            System.out.println("Sending response to client");
            System.out.println(respParam.getProducts().size() + " product(s) founded");
            response.getWriter().print(RequestHelper.responseToJSON(respParam));
        } else {
            System.out.println("No param found for search product");
            response.sendError(response.SC_BAD_REQUEST, "Important parameter needed");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        double cartSubtotal = 0.0;
        int numberOfProduct = 0;
        Cart cart = RequestHelper.getSessionCartAttribute(request);
        if(cart != null) {
            RequestHelper.setProductLineAttribute(request, cart.getProductLines());
            cartSubtotal = cart.getTotal();
            numberOfProduct = cart.getTotalProduct();
        }

        RequestHelper.setProductsAttribute(request, ProductService.getInstance().getAllProducts());
        RequestHelper.setDefaultFilterParameters(request);
        RequestHelper.setCartSubtotal(request, cartSubtotal);
        RequestHelper.setCartNumberOfProduct(request, numberOfProduct);
        RequestHelper.setCurrentPage(request, "product");
        request.getRequestDispatcher("/shop/product.jsp").forward(request, response);
    }
}
