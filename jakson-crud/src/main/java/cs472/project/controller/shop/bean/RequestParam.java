package cs472.project.controller.shop.bean;

import cs472.project.controller.shop.bean.CartProductParam;

import java.util.List;

public class RequestParam {
    private String type;
    private List<CartProductParam> data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<CartProductParam> getData() {
        return data;
    }

    public void setData(List<CartProductParam> data) {
        this.data = data;
    }

}
