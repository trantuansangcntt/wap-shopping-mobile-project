package cs472.project.controller.shop.bean;

import cs472.project.Entities.ProductLine;

import java.util.List;

public class ResponseParam {
    private int numberOfProduct;
    private List<ProductLine> productLines;

    public int getNumberOfProduct() {
        return numberOfProduct;
    }

    public void setNumberOfProduct(int numberOfProduct) {
        this.numberOfProduct = numberOfProduct;
    }

    public List<ProductLine> getProductLines() {
        return productLines;
    }

    public void setProductLines(List<ProductLine> productLines) {
        this.productLines = productLines;
    }
}
