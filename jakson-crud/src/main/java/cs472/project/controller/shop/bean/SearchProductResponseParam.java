package cs472.project.controller.shop.bean;

import cs472.project.Entities.Product;

import java.util.List;

public class SearchProductResponseParam {
    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
