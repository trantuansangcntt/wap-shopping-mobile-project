package cs472.project.controller.shop;

import com.fasterxml.jackson.databind.ObjectMapper;
import cs472.project.Entities.Cart;
import cs472.project.Entities.CartService;
import cs472.project.Entities.Product;
import cs472.project.Entities.ProductService;
import cs472.project.controller.shop.bean.RequestParam;
import cs472.project.controller.shop.bean.ResponseParam;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet("/shop/cart")
public class CartController extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestParam param = RequestHelper.getRequestParam(request, "param");
        if (param != null) {
            Cart cart = RequestHelper.getSessionCartAttribute(request);
            if (cart == null) {
                response.sendError(response.SC_INTERNAL_SERVER_ERROR, "something went wrong, please try again");
                System.out.println("Cart is not created yet for session");
                return;
            }

            switch (param.getType()) {
                case "UPDATE-CART":
                    cart.updateCart(param.getData());
                    break;
                case "ADD-PRODUCT-TO-CART":
                    cart.addProduct(param.getData().get(0).getProductId(), param.getData().get(0).getQty());
//                request.getRequestDispatcher("/shop/product.jsp").forward(request, response);
                    break;
                default:
                    response.sendError(response.SC_BAD_REQUEST, "Unknown request");
                    return;
            }
            ResponseParam respParam = RequestHelper.createResponseParam();
            respParam.setNumberOfProduct(cart.getTotalProduct());
            respParam.setProductLines(cart.getProductLines());

            for (int i = 0; i<cart.getProductLines().size(); i++){
                System.out.println("cart: "+cart.getProductLines().get(i).getProduct().getName());
                System.out.println("cart: "+cart.getProductLines().get(i).getProduct().getDescription());
                System.out.println("cart: "+cart.getProductLines().get(i).getProduct().getId());
                System.out.println("cart: "+cart.getProductLines().get(i).getProduct().getPrice());
                System.out.println("cart: "+cart.getProductLines().get(i).getProduct().getImage());
                System.out.println();
            }

            response.getWriter().print(RequestHelper.responseToJSON(respParam));
        } else {
            response.sendError(response.SC_BAD_REQUEST, "Important parameter needed");
        }

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        double cartSubtotal = 0.0;
        double cartTotal = 0.0;
        int numberOfProduct = 0;
        Cart cart = RequestHelper.getSessionCartAttribute(request);
        if (cart != null) {
            RequestHelper.setProductLineAttribute(request, cart.getProductLines());
            cartSubtotal = cart.getTotal();
            numberOfProduct = cart.getTotalProduct();
        }
        cartTotal = cartSubtotal;

        request.setAttribute("cartSubtotal", cartSubtotal);
        request.setAttribute("cartTotal", cartTotal);
        request.setAttribute("numberOfProduct", numberOfProduct);
        request.getRequestDispatcher("/shop/cart.jsp").forward(request, response);
    }
}
