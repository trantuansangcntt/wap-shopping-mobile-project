package cs472.project.controller;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cs472.project.Entities.ErrorResponse;
import cs472.project.Entities.User;

import javax.persistence.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet("/login")
public class LoginController extends HttpServlet {
    private static EntityManagerFactory emf;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("Login.jsp");
        view.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
//        resp.sendRedirect("checkout");
        try {
            emf = Persistence.createEntityManagerFactory("shoppingWAP");
        } catch (Throwable ex) {
            ex.printStackTrace();
            throw new ExceptionInInitializerError(ex);
        }
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();
        try {
            String userName = request.getParameter("username");
            String password = request.getParameter("password");
            String rememberMe = request.getParameter("remember");
            System.out.println("getParameter:  \nuserName: "+userName+"\npassword: "+password+"\nremember: "+rememberMe);
            HttpSession session= request.getSession();
//            System.out.println("userName: "+session.getAttribute("username")+
//                    "\npassword: "+session.getAttribute("password")+
//                    "\nrememberMe: "+session.getAttribute("remember"));

            ObjectMapper mapper = new ObjectMapper();
            User temp = mapper.readValue(request.getParameter("User"), User.class);
            System.out.println(temp);
            String email = temp.getEmail();
            String password_ = temp.getPassword();
            Query q =em.createQuery("from User u where u.email=:email");
            q.setParameter("email", email);
            List<User> users = q.getResultList();
            ErrorResponse errorResponse = new ErrorResponse();
            if (users != null && users.size()!=0 && users.get(0).getPassword().equals(password_)){

                errorResponse.setUrl("/checkout");
                errorResponse.setResponseCode("200");
//                resp.sendRedirect("/checkout");

//                Query getOrderItemList = em.createQuery("from OrderItem or where or.ownerId=:userId");
//                getOrderItemList.setParameter("userId",users.get(0).getId());
//                List<OrderItem> orderItems = getOrderItemList.getResultList();
//                session.setAttribute("orderItems",mapper.writeValueAsString(orderItems));

//                session.setAttribute();
            }else {
                errorResponse.setMessage("Wrong username or password");
            }
            PrintWriter out =resp.getWriter();
            try{
                out.print(mapper.writeValueAsString(errorResponse));
            }catch (JsonGenerationException e) {
                e.printStackTrace();
            }
            tx.begin();
            tx.commit();
        }catch (Throwable e) {
            System.err.println(e);
            if ((tx != null) && (tx.isActive())) tx.rollback();
        } finally {
            if ((em != null) && (em.isOpen())) em.close();
        }

        if (emf.isOpen()) {
            emf.close();
        }
    }
}
