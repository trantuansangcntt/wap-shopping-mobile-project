package cs472.project.controller;//package com.example.controller;


import cs472.project.Entities.OrderItem;
import cs472.project.Entities.User;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(value = "/sample" , loadOnStartup=1)
public class SampleDataController extends HttpServlet {

    @Override
    public void init() throws ServletException {
        System.out.println("SampleDataController running on startup");
        try {
            emf = Persistence.createEntityManagerFactory("shoppingWAP");
        } catch (Throwable ex) {
            ex.printStackTrace();
            throw new ExceptionInInitializerError(ex);
        }
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            List<User> users = em.createQuery("from User u where u.email='sang@abc.com'").getResultList();
            if (users == null || users.size()==0){
                User user = new User();
                user.setEmail("sang@abc.com");
                user.setAddress("1302 S Main Street, IA");
                user.setFullname("Tuan Sang Tran");
                user.setTelephone("831 233 456");
                user.setPassword("123");
                OrderItem or1 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhone6Plus",345.89,2,"1");
                OrderItem or2 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhone7Plus",700.55,1,"1");
                OrderItem or5 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhone8Plus",875.00,1,"1");
                OrderItem or6 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhone7",550.99,1,"1");
                OrderItem or3 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhone8",789.44,3,"1");
                OrderItem or4 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhoneX",999.00,2,"1");
                em.persist(user);
                em.persist(or1);
                em.persist(or2);
                em.persist(or3);
                em.persist(or4);
                em.persist(or5);
                em.persist(or6);
            }
            users = em.createQuery("from User u where u.email='bon@abc.com'").getResultList();
            if (users == null || users.size()==0){
                User user2 = new User();
                user2.setEmail("bon@abc.com");
                user2.setAddress("1000 N 1st, IA");
                user2.setFullname("Bon Nguyen");
                user2.setTelephone("223 345 223");
                user2.setPassword("123");
                OrderItem or1 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhone6Plus",345.89,2,"8");
                OrderItem or2 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhone7",550.99,1,"8");
                OrderItem or3 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhone8",789.44,3,"8");
                OrderItem or4 = createOrderItem("//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg",
                        "iPhoneX",999.00,2,"8");
                em.persist(user2);
                em.persist(or1);
                em.persist(or2);
                em.persist(or3);
                em.persist(or4);
            }
            tx.commit();
            users = em.createQuery("from User").getResultList();
            for (User p : users) {
                System.out.println("First name=" + p.getEmail()
                        + ", Last name= " + p.getPassword());
            }

        }catch (Throwable e) {
            System.err.println(e);
            if ((tx != null) && (tx.isActive())) tx.rollback();
        } finally {
            if ((em != null) && (em.isOpen())) em.close();
        }

        if (emf.isOpen()) {
            emf.close();
        }
    }

    private OrderItem createOrderItem(String imageURL, String prdName, double prdPrice, int quantity, String ownId){
        OrderItem orderItem = new OrderItem();
        orderItem.setProductImageURL(imageURL);
        orderItem.setProductName(prdName);
        orderItem.setProductPrice(prdPrice);
        orderItem.setProductQty(quantity);
        orderItem.setOwnerId(ownId);
        return orderItem;
    }

    private static EntityManagerFactory emf;
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        PrintWriter out =resp.getWriter();
        out.print("adfasdf");    }
}
