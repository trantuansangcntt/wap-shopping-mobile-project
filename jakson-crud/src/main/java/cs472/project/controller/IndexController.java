package cs472.project.controller;

import cs472.project.Entities.Cart;
import cs472.project.Entities.CartService;
import cs472.project.Entities.ProductService;
import cs472.project.controller.shop.RequestHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet("")
public class IndexController extends HttpServlet {
    @Override
    public void init() throws ServletException {
        ProductService.getInstance().loadProductsFromDB(getServletContext().getRealPath("/WEB-INF"));
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if(RequestHelper.getSessionCartAttribute(request) == null) {
            RequestHelper.createSessionCartAttribute(request);
        }

        super.service(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        RequestDispatcher view = request.getRequestDispatcher("checkout");
//        view.forward(request, response);
//        response.sendRedirect("HomeController");
//        response.sendRedirect("checkout");
//        response.sendRedirect("/shop/product");
        RequestHelper.initAttributesForRequest(request);
        RequestHelper.setCurrentPage(request, "");
        request.getRequestDispatcher("/index.jsp").forward(request, response);

    }
}
