package cs472.project.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;

public class OrderItemTag extends SimpleTagSupport {
    String price;
    String quantity;
    String imagepath;
    String name;

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setImagepath(String imagepath) {
        this.imagepath = imagepath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSeperate(String seperate) {
        this.seperate = seperate;
    }

    String seperate;

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public void doTag() throws JspException, IOException {
//        String imagePath = "//c1.staticflickr.com/1/466/19681864394_c332ae87df_t.jpg";
//        String name = "ABBBG";
//        String quantity = "4";
//        String price = "23.00";
//        JspWriter out = getJspContext().getOut();
//        out.println("My very first custom tag.");
//        out.println("<div class=\"form-group\"><hr /></div>");
//        if (name != null && imagePath != null && quantity != null && price != null){
        JspWriter out = getJspContext().getOut();
        out.println("<div class=\"form-group\">");
        out.println("<div class=\"col-sm-3 col-xs-3\">");
        out.println("<img class=\"img-responsive\" src=\"" + imagepath + "\" />");
        out.println("</div>");
        out.println("<div class=\"col-sm-6 col-xs-6\">");
        out.println("<div class=\"col-xs-12\">" + name + "</div>");
        out.println("<div class=\"col-xs-12\"><small>Quantity:<span>" + quantity + "</span></small></div>");
        out.println("</div>");
        out.println("<div class=\"col-sm-3 col-xs-3 text-right\">");
        out.println("<h6><span>$</span>" + price + "</h6>");
        out.println("</div>");
        out.println("</div>");
//            if (sperate != null){
        out.println("<div class=\"form-group\"><hr /></div>");
//            }
//        }
    }
}
