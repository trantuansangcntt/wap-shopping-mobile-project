package cs472.project.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MyTag extends SimpleTagSupport {
    public String getColor() {
        return color;
    }

    public String getSize() {
        return size;
    }

    String color;

    public void setColor(String color) {
        this.color = color;
    }

    public void setSize(String size) {
        this.size = size;
    }

    String size;
    @Override
    public void doTag() throws JspException, IOException {

        if (color != null && size != null){
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
            JspWriter out = getJspContext().getOut();
            out.println("<span style=\"color: "+color+"; font-size: "+size+";\">"+dNow+"</span>");
//            System.out.println("<span style=\"color: "+color+"; font-size: "+size+";\">"+dNow+"</span>");
        }else {
            JspWriter out = getJspContext().getOut();
            out.println("My very first custom tag.");
        }
    }
//    JspWriter jspout = getJspContext().getOut();
//    getJspContext().getOut().println("abc");
//    jspout
//    ("My very first custom tag.");
}
