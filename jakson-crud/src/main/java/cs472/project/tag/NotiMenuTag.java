package cs472.project.tag;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.JspTag;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;

public class NotiMenuTag extends SimpleTagSupport {
    boolean active;
    String href;
    String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public void doTag() throws JspException, IOException {
        JspWriter out = getJspContext().getOut();
        if(active) {
            out.println("<li class=\"sale-noti\">");
        } else {
            out.println("<li>");
        }

        out.println("<a href=" + href + ">" + text + "</a>");
        out.println("</li>");

//        if (color != null && size != null){
//            Date dNow = new Date();
//            SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");
//            JspWriter out = getJspContext().getOut();
//            out.println("<span style=\"color: "+color+"; font-size: "+size+";\">"+dNow+"</span>");
////            System.out.println("<span style=\"color: "+color+"; font-size: "+size+";\">"+dNow+"</span>");
//        }else {
//            JspWriter out = getJspContext().getOut();
//            out.println("My very first custom tag.");
//        }
    }
}
