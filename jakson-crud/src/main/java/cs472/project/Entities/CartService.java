package cs472.project.Entities;

public class CartService {
    private static CartService ourInstance = new CartService();

    public static CartService getInstance() {
        return ourInstance;
    }

    private CartService() {
    }

    public Cart createCart() {
        return new Cart();
    }
}
