package cs472.project.Entities;

import cs472.project.controller.shop.bean.CartProductParam;

import java.util.List;

public interface CartImpl {
    void addProduct(int productId, int qty);
    void removeProduct(int productId);
    void updateCart(List<CartProductParam> data) ;
    List<ProductLine> getListProductLine();
    int getQty();
    double getTotal();
    int getTotalProduct();
}
