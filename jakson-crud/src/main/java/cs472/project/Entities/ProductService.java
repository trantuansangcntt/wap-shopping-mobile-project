package cs472.project.Entities;

import cs472.project.controller.shop.bean.SearchProductParam;
import cs472.project.dao.ProductDAO;
import cs472.project.lib.util.JSONUtil;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ProductService {
    private static ProductService ourInstance = new ProductService();

    public static ProductService getInstance() {
        return ourInstance;
    }

    private ProductService() {
    }

    public void loadProductsFromDB(String path) {
        ProductDAO.getInstance().loadProductsDB(path + "/products");
    }

    public Product productFromJSON(String jsonObj) { return (Product) JSONUtil.objectFromJSON(jsonObj); }

    public String productToJSON(Product product) {
        return JSONUtil.objectToJSON(product);
    }

    public Product getProductById(int productId) {
        return ProductDAO.getInstance().getProductById(productId);
    }

    public List<Product> getAllProducts() {
        return ProductDAO.getInstance().getAllProducts();
    }

    public List<Product> searchProducts(SearchProductParam param) {
        List<String> criteria = Arrays.asList(param.getSearchText().split(" "));
//        List<Product> products = ProductDAO.getInstance().searchProducts(param.getSearchText());
        List<Product> result = ProductDAO.getInstance().getAllProducts();

        if(!param.getCategory().toLowerCase().equals("all")) {
            result = (List<Product>) result.stream().filter(p -> param.getCategory().toLowerCase().equals(p.getCategoryId())
            ).collect(Collectors.toList());
        }

        result = (List<Product>) result.stream().filter(p -> param.getPriceMin() <= p.getPrice() && p.getPrice() <= param.getPriceMax()
        ).collect(Collectors.toList());

        if(criteria.size() > 0) {
            result = (List<Product>) result.stream().filter(p -> {
                for (String s : criteria) {
                    if (p.getDescription().contains(s) || p.getName().contains(s)) {
                        return true;
                    }
                }
                return false;
            }).collect(Collectors.toList());
        }

         return result;
    }

}
