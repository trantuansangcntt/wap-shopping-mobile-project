package cs472.project.Entities;

import cs472.project.controller.shop.bean.CartProductParam;
import cs472.project.lib.util.JSONUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cart implements CartImpl {
    private Map<Integer, ProductLine> productLines;

    protected Cart() {
        productLines = new HashMap<>();
    }

    public List<ProductLine> getProductLines() {
        return new ArrayList<> (productLines.values());
    }

    public void setProductLines(Map<Integer, ProductLine> productLines) {
        this.productLines = productLines;
    }

    @Override
    public void addProduct(int productId, int qty) {
        ProductLine productLine = productLines.get(productId);
        if (productLine != null) {
            int newQty = productLine.getQty() + qty;
            productLine.setQty(newQty);
            productLine.setPrice(productLine.getProduct().getPrice() * newQty);
        } else {
            Product product = ProductService.getInstance().getProductById(productId);
            productLine = new ProductLine(product, product.getPrice() * qty, qty);
            productLines.put(productId, productLine);
        }
    }

    @Override
    public void removeProduct(int productId) {
        productLines.remove(productId);
    }

    @Override
    public int getQty() {
        return productLines.values().stream()
                .map( p -> p.getQty())
                .reduce(0, (x, y) -> x + y).intValue();
    }

    @Override
    public List<ProductLine> getListProductLine() {
        return new ArrayList<> (productLines.values());
    }

    @Override
    public double getTotal() {
        return productLines.values().stream().map( p -> p.getPrice()).reduce(0.0, (x, y) -> x + y).doubleValue();
    }

    @Override
    public int getTotalProduct() {
        return productLines.size();
    }

    @Override
    public void updateCart(List<CartProductParam> data) {
        if(data != null) {
            productLines.clear();
            data.forEach(d -> addProduct(d.getProductId(), d.getQty()));
        }
    }

    @Override
    public String toString() {
        return JSONUtil.objectToJSON(this);
    }

}
