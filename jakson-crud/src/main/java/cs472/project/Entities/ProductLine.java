package cs472.project.Entities;

import cs472.project.lib.util.JSONUtil;

public class ProductLine {
	private Product product;
	private double price;
	private int qty;

	protected ProductLine(Product product, double price, int qty) {
		this.product = product;
		this.price = price;
		this.qty = qty;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public double getPrice() {
		return price;
	}

	public int getQty() {
		return qty;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	@Override
	public String toString() {
		return JSONUtil.objectToJSON(this);
	}
}
