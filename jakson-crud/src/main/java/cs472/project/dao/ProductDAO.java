package cs472.project.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import cs472.project.Entities.Product;
import cs472.project.Entities.ProductService;
import cs472.project.lib.util.JSONUtil;


public class ProductDAO {
    Map<Integer, Product> productsDb;

    private static ProductDAO ourInstance = new ProductDAO();
    String productJsonFile;

    public static ProductDAO getInstance() {
        return ourInstance;
    }

    private ProductDAO() {

    }

    public void loadProductsDB(String filepath) {
        try {
            productJsonFile = filepath;
            productsDb = new HashMap<>();
            fromJSON(JSONUtil.readJSONFile(productJsonFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void fromJSON(HashMap<String, Object> jsonMap) {
        if(jsonMap != null) {
            jsonMap.forEach((key, obj) -> {
                productsDb.put(Integer.parseInt(key), ProductService.getInstance().productFromJSON(JSONUtil.objectToJSON(obj)));
            });
        }
    }

    private HashMap<String, Object> toJSON() {
        HashMap<String, Object> objects = new HashMap<>();
        productsDb.forEach((key, obj) -> {
            objects.put(key.toString(), obj);
        });
        return objects;
    }


    public void addProduct(Product product) {
        try {
            productsDb.put(product.getId(), product);
            JSONUtil.writeJSONFile(productJsonFile, toJSON());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteProduct(int productId) {
        productsDb.remove(productId);
    }

    public void updateProduct(Product product) {
        productsDb.put(product.getId(), product);
    }

    public List<Product> getAllProducts() {
        return new ArrayList<>(productsDb.values());
    }

    public Product getProductById(int productId) {
        return productsDb.get(productId);
    }

    public int genId() {
        return productsDb.size() + 1;
    }

    public List<Product> searchProducts(String searchText) {
        return productsDb.values().stream().filter( p -> p.getName().contains(searchText)
                || p.getDescription().contains(searchText)).collect(Collectors.toList());
    }
}
