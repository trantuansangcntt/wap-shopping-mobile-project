package cs472.project.lib.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import cs472.project.Entities.Product;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class JSONUtil {
    public static HashMap<String,Object> readJSONFile(String filename) throws IOException {
        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(filename);
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(inputStream));

            StringBuilder result = new StringBuilder();
            String line;

            // read each line of the stream
            while ((line = streamReader.readLine()) != null) {
                result.append(line);
            }
            streamReader.close();

            return new ObjectMapper().readValue(new StringReader(result.toString()), HashMap.class);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }

        }

        return null;
    }

    public static void writeJSONFile(String filename, HashMap<String, Object> objects) throws IOException {
        File file = new File(filename);
        FileOutputStream fileOutputStream = null;

        try {

            fileOutputStream = new FileOutputStream(file);
            if (!file.exists()) {
                file.createNewFile();
            }

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.writeValue(fileOutputStream, objects);

            // Flush and close file output streams
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fileOutputStream != null) {
                fileOutputStream.close();
            }
        }

    }

    public static String objectToJSON(Object obj) {
        String result = "";
        try {
            result = new ObjectMapper().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static Object objectFromJSON(String jsonObj) {
        ObjectMapper mapper = new ObjectMapper();
        Object object = null;
        try {
            object = mapper.readValue(jsonObj, Product.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return object;
    }
}
