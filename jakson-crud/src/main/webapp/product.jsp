<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Add New Contact</title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="resources/js/script.js"></script>
</head>
<body>
	<table id="tbl_products">
		<thead>
			<tr>
				<th>Id</th>
				<th>Name</th>
				<th>Price</th>
				<th>Image</th>
				<th>Description</th>
				<th>Amount Available</th>
				<th>Category</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${products}" var="product">
				<tr>
					<td><c:out value="${product.id}" /></td>
					<td><c:out value="${product.name}" /></td>
					<td><c:out value="${product.price}" /></td>
					<td><c:out value="${product.image}" /></td>
					<td><c:out value="${product.description}" /></td>
					<td><c:out value="${product.amountAvailable}" /></td>
					<td><c:out value="${product.categoryId}" /></td>
					<td><c:out value="${product.active}" /></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>


	<fieldset>
		<div>
            <label for="product_id">Id</label> <input type="text" id="product_id"
				readonly="readonly" placeholder="Id" />
		</div>
		<div>
			<label for="product_name">Name</label> <input type="text" id="product_name"
				placeholder="Name" />
		</div>
		<div>
			<label for="product_price">Price</label> <input type="text" id="product_price"
				placeholder="Price" />
		</div>

        <div>
            <label for="product_image">Image</label> <input type="text" id="product_image"
                                              placeholder="Product picture" />
        </div>
        <div>
            <label for="product_description">Description</label> <input type="text" id="product_description"
                                                  placeholder="Description" />
        </div>
        <div>
            <label for="product_availableAmount">Available amount</label> <input type="text" id="product_availableAmount"
                                                    placeholder="Available amount" />
        </div>
        <div>
            <label for="product_categoryId">Category</label> <input type="text" id="product_categoryId"
                                                                                 placeholder="CategoryId" />
        </div>

        <div>
            <label for="product_active">Active</label> <input type="text" id="product_active"
                                                    placeholder="active" />
        </div>

        <div>
            <input id="btnAddProduct" type="submit" value="Submit" />
        </div>
	</fieldset>
</body>
</html>