<%--
  Created by IntelliJ IDEA.
  User: bonng
  Date: 7/13/18
  Time: 23:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Product</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="../resources/images/icons/favicon.png"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/fonts/themify/themify-icons.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/fonts/elegant-font/html-css/style.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/slick/slick.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/vendor/noui/nouislider.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="../resources/css/util.css">
    <link rel="stylesheet" type="text/css" href="../resources/css/main.css">
    <!--===============================================================================================-->

<body class="animsition">

<!-- Header -->
<jsp:include page="/shop/header.jsp"/>

<!-- Title Page -->
<section class="bg-title-page p-t-50 p-b-40 flex-col-c-m"
         style="background-image: url(../resources/images/heading-pages-02.jpg);">
    <h2 class="l-text2 t-center">
        Mobile Shop
    </h2>
    <p class="m-text13 t-center">
        New Arrivals
    </p>
</section>


<!-- Content page -->
<section class="bgwhite p-t-55 p-b-65">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg-3 p-b-50">
                <div class="leftbar p-r-20 p-r-0-sm">
                    <!--  -->
                    <h4 class="m-text14 p-b-7">
                        Categories
                    </h4>

                    <ul class="p-b-54">
                        <li class="p-t-4">
                            <span class="bntAll s-text13 active1">
                                All
                            </span>
                        </li>

                        <li class="p-t-4">
                            <span class="bntPhone s-text13">
                                Phone
                            </span>
                        </li>

                        <li class="p-t-4">
                            <span class="bntTablet s-text13">
                                Tablet
                            </span>
                        </li>
                    </ul>

                    <!--  -->
                    <h4 class="m-text14 p-b-32">
                        Filters
                    </h4>

                    <div class="filter-price p-t-22 p-b-50 bo3">
                        <div class="m-text15 p-b-17">
                            Price
                        </div>

                        <div class="wra-filter-bar">
                            <div id="filter-bar"></div>
                        </div>

                        <div class="flex-sb-m flex-w p-t-16">
                            <div class="w-size11">
                                <!-- Button -->
                                <button id="btnFilterProduct"
                                        class="flex-c-m size4 bg7 bo-rad-15 hov1 s-text14 trans-0-4">
                                    Filter
                                </button>
                            </div>

                            <div class="s-text3 p-t-10 p-b-10">
                                Range: $<span id="value-lower">${filterMin}</span> - $<span
                                    id="value-upper">${filterMax}</span>
                            </div>
                        </div>
                    </div>

                    <div class="search-product pos-relative bo4 of-hidden">
                        <input id="txtSearch" class="s-text7 size6 p-l-23 p-r-50" type="text" name="search-product"
                               placeholder="Search Products...">

                        <button class="bntSearch flex-c-m size5 ab-r-m color2 color0-hov trans-0-4">
                            <i class="fs-12 fa fa-search" aria-hidden="true"></i>
                        </button>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-md-8 col-lg-9 p-b-50">
                <!--  -->
                <div class="flex-sb-m flex-w p-b-35">
                    <div class="flex-w">
                        <div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
                            <select class="selection-2" name="sorting">
                                <option>Default Sorting</option>
                                <option>Popularity</option>
                                <option>Price: low to high</option>
                                <option>Price: high to low</option>
                            </select>
                        </div>

                        <div class="rs2-select2 bo4 of-hidden w-size12 m-t-5 m-b-5 m-r-10">
                            <select class="selection-2" name="sorting">
                                <option>Price</option>
                                <option>$0.00 - $50.00</option>
                                <option>$50.00 - $100.00</option>
                                <option>$100.00 - $150.00</option>
                                <option>$150.00 - $200.00</option>
                                <option>$200.00+</option>

                            </select>
                        </div>
                    </div>

                    <span class="s-text8 p-t-5 p-b-5">
							Showing 1–12 of ${totalProduct} results
						</span>
                </div>

                <!-- Product -->
                <div id="productsContainer" class="row">
                    <c:forEach items="${products}" var="product">
                        <div class="col-sm-12 col-md-6 col-lg-4 p-b-50">
                            <!-- Block2 -->
                            <div class="block2">
                                <div class="block2-img wrap-pic-w of-hidden pos-relative block2-labelnew">
                                    <img src="..${product.image}" alt="IMG-PRODUCT">

                                    <div class="block2-overlay trans-0-4">
                                        <a href="#" class="block2-btn-addwishlist hov-pointer trans-0-4">
                                            <i class="icon-wishlist icon_heart_alt" aria-hidden="true"></i>
                                            <i class="icon-wishlist icon_heart dis-none" aria-hidden="true"></i>
                                        </a>

                                        <div productId="${product.id}" class="block2-btn-addcart w-size1 trans-0-4">
                                            <!-- Button -->
                                            <button class="flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4">
                                                Add to Cart
                                            </button>
                                        </div>
                                        s
                                    </div>
                                </div>

                                <div class="block2-txt p-t-20">
                                    <a href="/shop/product-detail?productId=${product.id}"
                                       class="block2-name dis-block s-text3 p-b-5">
                                            ${product.name}
                                    </a>

                                    <span class="block2-price m-text6 p-r-5">
                                            $${product.price}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </c:forEach>

                    <!-- Pagination -->
                    <div class="pagination flex-m flex-w p-t-26">
                        <a href="#" class="item-pagination flex-c-m trans-0-4 active-pagination">
                            <c:if test="${totalProduct < 20}">
                                <c:out value="1"/>
                            </c:if>
                            <c:if test="${totalProduct > 20}">
                                <c:out value="${totalProduct / 20}"/>
                            </c:if>
                        </a>
                    </div>
                </div>
            </div>
        </div>
</section>


<!-- Footer -->
<jsp:include page="/shop/footer.jsp"/>

<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/bootstrap/js/popper.js"></script>
<script type="text/javascript" src="../resources/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/select2/select2.min.js"></script>
<script type="text/javascript">
    $(".selection-1").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect1')
    });

    $(".selection-2").select2({
        minimumResultsForSearch: 20,
        dropdownParent: $('#dropDownSelect2')
    });
</script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="../resources/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/slick/slick.min.js"></script>
<script type="text/javascript" src="../resources/js/slick-custom.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/sweetalert/sweetalert.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/vendor/noui/nouislider.min.js"></script>
<!--===============================================================================================-->
<script type="text/javascript" src="../resources/js/shop/header.js"></script>
<script type="text/javascript" src="../resources/js/shop/product.js"></script>
<!--===============================================================================================-->

<script src="../resources/js/main.js"></script>

</head>

</body>
</html>

