$(function(){
	$('#btnAddProduct').click(addProduct);
	
	function addProduct(){
		var productName = $('#txtName').val();
		var productPrice = $('#txtPrice').val();
        var productImage = $('#txtImage').val();
        var productDescription = $('#txtDescription').val();
        var productAmountAvailable = $('#txtAmountAvailable').val();
        var productCategoryId = $('#txtCategoryId').val();
        var productActive = $('#chkActive').val() === 'checked' ? 1 : 0;
		var product = {
			name:productName,
			price:productPrice,
            image:productImage,
            description:productDescription,
            amountAvailable:productAmountAvailable,
            categoryId:productCategoryId,
            active:productActive
		};
		$.post('/admin/product',{product: JSON.stringify(product)}, processData, "json")
	}

	function processData(data){
		//data = JSON.parse(data);
		var td0 = $('<td>').text(data.id);
		var td1 = $('<td>').text(data.name);
		var td2 = $('<td>').text(data.price);
        var td3 = $('<td>').text(data.image);
        var td4 = $('<td>').text(data.description);
        var td5 = $('<td>').text(data.amountAvailable);
        var td6 = $('<td>').text(data.categoryId);
        var td7 = $('<td>').text(data.active);

		var tr = $('<tr>').append(td0).append(td1).append(td2).append(td3).append(td4).append(td5).append(td6).append(td7);
		$('#tbl_products>tbody').append(tr);
	}
})

