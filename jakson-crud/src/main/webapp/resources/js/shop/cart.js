$(function () {

    $('.btnUpdateCart').each(function () {
        $(this).on('click', function () {
            var param = {
                type: "UPDATE-CART",
                data: [],
            };
            $(".txtProductQty").each(function (key, value) {
                param.data.push({
                    productId: $(this).attr("productId"),
                    qty: $(this).val()
                });
            });
            console.log(param);
            $.post("/shop/cart", {param: JSON.stringify(param)})
                .done(function (data) {
                    let resp = JSON.parse(data);
                    let cartSubTotal = 0.0;
                    let cartTotal = 0.0;
                    console.log(resp);
                    $(".numProduct").text(resp.numberOfProduct);
                    $(".txtProductQty").each(function (key, value) {
                        let productId = $(this).attr("productId");
                        for (let k in resp.productLines) {
                            let price = parseFloat(resp.productLines[k].product.price);
                            let qty = resp.productLines[k].qty;
                            let total = price * qty;
                            if(productId == resp.productLines[k].product.id ) {
                                cartSubTotal = cartSubTotal + total;
                            }
                            $("#txtProductPrice-" + resp.productLines[k].product.id).html("$" + price.toFixed(2));
                            $("#txtProductTotal-" + resp.productLines[k].product.id).html("$" + total.toFixed(2));
                            $("#txtProductQty-" + resp.productLines[k].product.id).val(qty);
                        }
                    });
                    cartTotal = cartSubTotal;
                    $("#cartSubTotal").html("$" + cartSubTotal.toFixed(2));
                    $("#cartTotal").html("$" + cartTotal.toFixed(2));
                    updateMenuCheckout(resp.productLines);
                    swal("SUCCESS", "Your cart is updated !", "success");
                })
                .fail(function () {
                    swal("FAILED", "Failed to update your cart !", "error");
                });
        });
    });

    $('.btnRemoveProduct').each(function () {
        $(this).on('click', function () {
            let rowId = $(this).attr("productId");
            $("#row-" + rowId).remove();
        });
    });

    $('.btnCheckout').each(function () {
        $(this).on('click', function () {
            window.location.href = "/login";
        });
    });
})