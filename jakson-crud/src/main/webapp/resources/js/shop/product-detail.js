$(function () {
    $('.btn-addcart-product-detail').each(function () {
        var nameProduct = $(".product-detail-name").html();
        $(this).on('click', function () {
            let param = {
                type: "ADD-PRODUCT-TO-CART",
                data: [{productId: $(this).attr("productId"), qty: $(".txtProduct").val()}]
            };

            $.post("/shop/cart", {param: JSON.stringify(param)})
                .done(function (data) {
                    $(".numProduct").text(JSON.parse(data).numberOfProduct);
                    swal(nameProduct, "is added to cart !", "success");
                    updateMenuCheckout(JSON.parse(data).productLines);
                })
                .fail(function () {
                    swal(nameProduct, "is failed to add to cart !", "error");
                });
        });
    });

    $('.block2-btn-addwishlist').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });
})