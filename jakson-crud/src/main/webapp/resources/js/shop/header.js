function updateMenuCheckout(productLines) {
    let carSubTotal = 0.0;
    $("#cartMenu").prepend(createCartSubTotalMenu());
    for (var k in productLines) {
        carSubTotal = parseFloat(productLines[k].product.price) * productLines[k].qty;
        $("#cartMenu").prepend(addProductLine(productLines[k]));
    }
    updateCartSubTotalMenu(carSubTotal);
}

function addProductLine(productLine) {
    let productPrice = parseFloat(productLine.product.price);
    let qty = productLine.qty;
    return (
        $("<div>").addClass("header-cart header-dropdown").append(
            $("<ul>").addClass("header-cart-wrapitem").append(
                $("<li>").addClass("header-cart-item").append(
                    $("<div>").addClass("header-cart-item-img").append(
                        $("<img>").attr({src: ".." + productLine.product.image, alt:"IMG-PRODUCT"})
                    )
                ).append(
                    $("<div>").addClass("header-cart-item-txt").append(
                        $("<a>").addClass("header-cart-item-name").attr({href: "#"}).text(productLine.product.name)
                    ).append(
                        $("<span>").addClass("header-cart-item-info").text("$" + productPrice.toFixed(2) + " x " + qty)
                    )
                )
            )
        )
    );
}

function createCartSubTotalMenu() {
    return $("<div>").addClass("header-cart-total").text("Total: $0.00");
}

function updateCartSubTotalMenu(cartSubTotal) {
    return $(".header-cart-total").text("Total: $" + cartSubTotal.toFixed(2));
}

function updateCartQty(numberOfProduct) {
    $(".cartQty").text(numberOfProduct);
}

