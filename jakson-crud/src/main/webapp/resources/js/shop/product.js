$(function () {
    /*[ No ui ]
    ===========================================================*/
    var filterBar = $('#filter-bar')[0];
    var min = parseInt($('#value-lower').text());
    var max = parseInt($('#value-upper').text());

    noUiSlider.create(filterBar, {
        start: [min, max],
        connect: true,
        range: {
            'min': min,
            'max': max
        }
    });

    var skipValues = [$('#value-lower')[0], $('#value-upper')[0]];

    filterBar.noUiSlider.on('update', function (values, handle) {
        skipValues[handle].innerHTML = Math.round(values[handle]);
    });

    $('.block2-btn-addcart').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            let param = {
                type: "ADD-PRODUCT-TO-CART",
                data: [{productId: $(this).attr("productId"), qty: 1}]
            };

            $.post("/shop/cart", {param: JSON.stringify(param)})
                .done(function (data) {
                    console.log("received response:", JSON.parse(data));
                    $(".numProduct").text(JSON.parse(data).numberOfProduct);
                    swal(nameProduct, "is added to cart !", "success");
                    updateMenuCheckout(JSON.parse(data).productLines);
                })
                .fail(function () {
                    swal(nameProduct, "is failed to add to cart !", "error");
                });
        });
    });

    $('.block2-btn-addwishlist').each(function () {
        var nameProduct = $(this).parent().parent().parent().find('.block2-name').html();
        $(this).on('click', function () {
            swal(nameProduct, "is added to wishlist !", "success");
        });
    });

    $(".bntSearch").each(function () {
        $(this).on('click', function () {
            let param = {
                searchText: $("#txtSearch").val(),
                priceMin: parseInt($('#value-lower').text()),
                priceMax: parseInt($('#value-upper').text()),
                category: "all"
            };
            console.log("Start searching with param: ", JSON.stringify(param));
            $.post("/shop/product", {param: JSON.stringify(param)}, function () {
                console.log("Sent search request to server: ", JSON.stringify(param));
                $("#productsContainer").empty();
            }).done(function (data) {
                console.log("Update product container: ", JSON.parse(data));
                let products = JSON.parse(data).products;
                if (products.length > 0) {
                    for (var k in products) {
                        $("#productsContainer").append(createProductView(products[k]));
                    }

                    $("#productsContainer").append(addPagination(products.length));
                } else {
                    $("#productsContainer").append($("<span>").text("No result found."));
                }
            }).fail(function () {
                swal("ERROR", "Something went wrong. Please try again !", "error");
            });
        });
    })

    function createProductView(product) {
        return (
            $("#productsContainer").append(
                $("<div>").addClass("col-sm-12 col-md-6 col-lg-4 p-b-50").append(
                    $("<div>").addClass("block2").append(
                        $("<div>").addClass("block2-img wrap-pic-w of-hidden pos-relative block2-labelnew").append(
                            $("<img>").attr({src: ".." + product.image, alt: "IMG-PRODUCT"})
                        ).append(
                            $("<div>").addClass("block2-overlay trans-0-4").append(
                                $("<a>").addClass("block2-btn-addwishlist hov-pointer trans-0-4").attr({href: "#"}).append(
                                    $("<i>").addClass("icon-wishlist icon_heart_alt")
                                        .attr({"aria-hidden": "true"})
                                ).append(
                                    $("<i>").addClass("icon-wishlist icon_heart dis-none")
                                        .attr({"aria-hidden": "true"})
                                )
                            ).append(
                                $("<div>").addClass("block2-btn-addcart w-size1 trans-0-4").attr({productId: product.id}).append(
                                    $("<button>").addClass("flex-c-m size1 bg4 bo-rad-23 hov1 s-text1 trans-0-4").text("Add to Cart")
                                )
                            )
                        )
                    ).append(
                        $("<div>").addClass("block2-txt p-t-20").append(
                            $("<a>").addClass("block2-name dis-block s-text3 p-b-5")
                                .attr({href: "/shop/product-detail?productId=" + product.id})
                                .text(product.name)
                        ).append(
                            $("<span>").addClass("block2-price m-text6 p-r-5").text("$" + product.price.toFixed(2))
                        )
                    )
                )
            )
        );
    }

    function addPagination(totalProduct) {
        let pages = 1;
        if (totalProduct > 20) {
            pages = totalProduct / 20;
        }
        return $("<div>").addClass("pagination flex-m flex-w p-t-26").append(
            $("<a>").addClass("item-pagination flex-c-m trans-0-4 active-pagination")
                .attr({href: "#"})
                .text(pages)
        );
    }
})

